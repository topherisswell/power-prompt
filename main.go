package main

// symbols
// ⚛ ⚚ ⚙ ♲ ♻ ♼ ♽
// ♪ 🎝 ⛈ ⛓ ⛰ ⛱ ✇ ✎ 🌐 🌀 🗺
// 🌡 🌣 🌤 🌥 🌦 🌧 🌨 🌩 🌪 🌫 🌬
// 🌑 🌒 🌓 🌔 🌕 🌖 🌗 🌘
// 🎛 🎚 🎟 🎞 📾 📽 🕊
// 🕨 🕩 🕪 🕮 🕯 🕰 🕱 🕷 🕸
// 🕾 🕿 🖂 🖈 🖉 🖫 🖧 🖳 🖥 🖵 🖴
// 🗈 🗊 🗅 🗇 🗝 🗡 🗨 🔋 🗱 🗲 🗹 🗸 🗷 🗴 🗆
import (
	"context"
	"crypto/sha256"
	"fmt"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	appName       = "power-prompt"
	envPrefix     = "POWERPROMPT"
	appVersion    = "0.1.3"
	appGitHash    string
	appGitBranch  string
	appBuildTime  string
	defaultPS1    = "\\u\\@\\h \\W\\$ "
	homeDir       string
	k8sConfigFile string
	//\n\[\e[32;1m\],-[ \[\e[0m\]
	// the brackets ensure that bash can count characters correctly
	Reset  = "\\[\\e[0m\\]"
	Red    = "\\[\\e[31m\\]"
	Green  = "\\[\\e[32m\\]"
	Yellow = "\\[\\e[33m\\]"
	Blue   = "\\[\\e[34m\\]"
	Purple = "\\[\\e[35m\\]"
	Cyan   = "\\[\\e[36m\\]"
	Gray   = "\\[\\e[37m\\]"
	White  = "\\[\\e[97m\\]"
	/* 	Reset      = "\\e[0m"
	   	Red        = "\\e[31m"
	   	Green      = "\\e[32m"
	   	Yellow     = "\\e[33m"
	   	Blue       = "\\e[34m"
	   	Purple     = "\\e[35m"
	   	Cyan       = "\\e[36m"
	   	Gray       = "\\e[37m"
	   	White      = "\\e[97m" */
	lineNumber = 0
)

func main() {

	var ps1 string = defaultPS1
	var k8sstr string
	var tfstr string
	var awsstr string
	var gitstr string
	var hoststr string
	// the timeout in milliseconds unless overridden by the user
	var defaultTimeout = 500
	var err error

	// whenever we return, dump the prompt
	defer func() {
		ps1 = writeLine(k8sstr) + writeLine(tfstr) +
			writeLine(awsstr) + writeLine(gitstr) +
			writeLine(hoststr) + ps1
		fmt.Printf("\n%s", ps1)

	}()

	// print version in debug mode
	verString := appName + " v" + appVersion
	if len(appBuildTime) > 0 {
		verString += fmt.Sprintf(" Built at %s", appBuildTime)
	}
	if len(appGitBranch) > 0 {
		verString += fmt.Sprintf(" Ref %s", appGitBranch)
	}
	if len(appGitHash) > 0 {
		verString += fmt.Sprintf(" Commit %s", appGitHash)
	}
	LogDebug(verString)

	// how long the whole program has to execute before it _must_ return, whether or not it has a prompt
	// once this expires, it prints whatever we have and exits
	timeoutMilliseconds, err := strconv.Atoi(GetSettingOrSetDefault("TIMEOUT_MS", fmt.Sprintf("%d", defaultTimeout)))
	if err != nil {
		LogError("Unable to convert TIMEOUT_MS parameter to integer. Value looks like it's " + GetSetting("TIMEOUT_MS"))
		timeoutMilliseconds = defaultTimeout
	}
	// Sanitycheck on the timeout
	// must be at least 2 milliseconds to allow a 1 ms warning buffer
	if timeoutMilliseconds < 2 {
		timeoutMilliseconds = 2
	}

	// when this context expires, print the PS1 and exit
	bigctx, canx := context.WithTimeout(context.Background(), time.Millisecond*time.Duration(timeoutMilliseconds))
	defer canx()

	// how many ms between when the threads are signaled to end and when the whole program returns
	// this is how long the threads have to wrap up
	// set to 90% and then do bounds checking to make sure it's between 1 and 20ms
	var timeoutWarningBufferMilliseconds int = timeoutMilliseconds / 10

	if timeoutWarningBufferMilliseconds > 20 {
		timeoutWarningBufferMilliseconds = 20
	}
	if timeoutWarningBufferMilliseconds > timeoutMilliseconds {
		timeoutWarningBufferMilliseconds = timeoutMilliseconds - 1
	}
	if timeoutWarningBufferMilliseconds < 1 {
		timeoutWarningBufferMilliseconds = 1
	}

	// how long the context for threads lasts. This should be <= timeoutMilliseconds
	var threadTimeoutMilliseconds = timeoutMilliseconds - timeoutWarningBufferMilliseconds
	if threadTimeoutMilliseconds > timeoutMilliseconds {
		threadTimeoutMilliseconds = timeoutMilliseconds - 1
	}
	if threadTimeoutMilliseconds < 1 {
		threadTimeoutMilliseconds = 1
	}

	// This context tells every other routine to cleanup and dump whatever they have
	// they'll have ~10ms before exiting
	ctx, littlecanx := context.WithTimeout(bigctx, time.Millisecond*time.Duration(threadTimeoutMilliseconds))
	defer littlecanx()

	homeDir, err = os.UserHomeDir()
	if err != nil {
		// this doesn't do anything either
		os.Setenv("PS1", defaultPS1)
		return
	}

	// wait until all threads are finished
	// sent to a goroutine so as to not block the context expiry
	var wg sync.WaitGroup

	k8schan := make(chan string)
	wg.Add(1)
	go func() {
		start := time.Now()
		writeK8sLine(ctx, k8schan, &wg)
		LogDebug("K8s - complete in " + time.Since(start).String())
	}()

	tfchan := make(chan string)
	wg.Add(1)
	go func() {
		start := time.Now()
		writeTerraformLine(ctx, tfchan, &wg)
		LogDebug("TF - complete in " + time.Since(start).String())
	}()

	awschan := make(chan string)
	wg.Add(1)
	go func() {
		start := time.Now()
		writeAWSLine(ctx, awschan, &wg)
		LogDebug("AWS - complete in " + time.Since(start).String())
	}()

	gitchan := make(chan string)
	wg.Add(1)
	go func() {
		start := time.Now()
		writeGitLine(ctx, gitchan, &wg)
		LogDebug("Git - complete in " + time.Since(start).String())
	}()

	hostchan := make(chan string)
	wg.Add(1)
	go func() {
		start := time.Now()
		writeHostInfoLine(ctx, hostchan, &wg)
		LogDebug("HostInfo - complete in " + time.Since(start).String())
	}()

	ps1 = writePromptLine(ctx)
	softWait := SoftWait(&wg)
	for {
		select {
		case <-bigctx.Done():
			LogError("Timed out")
			return
		case k8sstr = <-k8schan:
			continue
		case tfstr = <-tfchan:
			continue
		case awsstr = <-awschan:
			continue
		case hoststr = <-hostchan:
			continue
		case gitstr = <-gitchan:
			continue
		case <-softWait:
			LogDebug("Everything returned")
			return
		}
	}

}

func SetSetting(settingName string, settingValue string) {

	os.Setenv(strings.ToUpper(envPrefix+"_"+settingName), settingValue)
}

func GetSetting(settingName string) string {
	return os.Getenv(strings.ToUpper(envPrefix + "_" + settingName))
}

// GetSettingOrSetDefault will get a setting if it exists and set it to a
// user-supplied value if it doesn't and return that default value back
// only sets the value for this execution. It cannot export it to the users'
// environment for subsequent executions
// usage (this example will set timeout to the env var or to 400 if it doesn't exist):
// timeout = GetSettingOrSetDefault("TIMEOUT", 400)
func GetSettingOrSetDefault(settingName string, defaultVal string) string {
	currentVal := GetSetting(settingName)
	if len(currentVal) <= 0 {
		SetSetting(settingName, defaultVal)
		return defaultVal
	} else {
		return currentVal
	}
}

func GetFlag(optionString string) bool {
	val := strings.ToUpper(GetSetting(optionString))
	if val == "1" || val == "TRUE" {
		return true
	}
	return false
}

func SetFlag(optionString string, optionValue bool) {
	val := "1"
	if !optionValue {
		val = "0"
	}
	SetSetting(optionString, val)
}

func LogDebug(msg string) {
	if GetFlag("DEBUG") {
		fmt.Fprintf(os.Stderr, "\nDEBUG %s", msg)
	}
}

func LogError(msg string) {
	if GetFlag("DEBUG") {
		fmt.Fprintf(os.Stderr, "\nERROR %s", msg)
	}
}

// SoftWait() takes a wait group and returns a channel
// It will close the channel once the waitgroup is done
// thus allowing you to check if a waitgroup is complete
// without having to block if its not
func SoftWait(wg *sync.WaitGroup) <-chan struct{} {
	c := make(chan struct{})
	go func() {
		defer close(c)
		wg.Wait()
	}()
	return c
}

func writeLine(line string) (str string) {
	if len(line) > 0 {
		if lineNumber < 1 {
			str = fmt.Sprintf("%s╓%s%s\n", Green, Reset, line)
		} else {

			str = fmt.Sprintf("%s╟%s%s\n", Green, Reset, line)
		}
		lineNumber++

	}
	return
}

func writePromptLine(ctx context.Context) (str string) {
	promptColor := ""
	lastRC, lastRCSet := os.LookupEnv("LAST_RC")
	if lastRCSet && lastRC != "0" {
		promptColor = Red
	}
	nixStr := ""
	shellInfo := ""
	// TODO - Shell is sometimes in the SH env var
	shell, shellSet := os.LookupEnv("SHELL")
	// this makes a few assumptions by checking if
	// $SHELL=/nix/blah... but I _think_ this covers
	// the default install on all compatible OSes.
	if shellSet && len(shell) > 0 {
		if strings.HasPrefix(shell, "/nix") {
			// we're in a nix-shell
			nixStr = "❄"
		}
		// returns the executable name of the shell
		shellInfo = path.Base(shell)
	}

	str = fmt.Sprintf("%s―%s―%s―%s\\$%s ", Green, nixStr, shellInfo, promptColor, Reset)
	return
}

// highlightHash will take a string to be printed, and return
// the string with ANSI coloring (back- and foreground). The
// coloring is based on the hash of the string, so different
// strings produce different coloring. This allows you to spot
// differences in a given value at a glance (maybe). There's
// only 91 combinations, so it's nothing bullet-proof (obvs).
// TODO - This doesn't guarantee the string will be readable!
func highlightHash(str string) string {

	h := sha256.New()
	h.Write([]byte(str))
	hashsum := h.Sum(nil)
	textColor := hashsum[0] % 10
	var bgColor byte
	if textColor == 9 {
		// if the textColor is default
		// we don't care if bgColor is also default
		bgColor = hashsum[1] % 10
	} else {
		bgColor = hashsum[1]%9 + 1
		if textColor == bgColor {
			// back- and fore-ground are the same color
			bgColor -= 1
		}
	}
	return fmt.Sprintf("\\[\\e[3%d;4%dm\\]%s%s", textColor, bgColor, str, Reset)
}

// FormatHighlightableString will return a formatted string. You provide a namespace for the
// string (this is just a name unique throughout the program like "AWS_USER_ID") and the string
// to be formatted. It will check if there is a HIGHLIGHT_HASH env var enabled for that namespace
// example: POWERPROMPT_AWS_USER_ID_HIGHLIGHT_HASH=true
// if so, it will hash the string and make a deterministic highlight pattern
// if not, it will format the string with the "normalFormat" provided.
// usage:
// awsUserIDFormattedString := FormatHighlightableString(awsUserId, "AWS_USER_ID", Yellow)
func FormatHighlightableString(str string, namespace string, normalFormat string) string {
	// don't do anything if an empty string is passed
	if len(str) <= 0 {
		return ""
	}
	// check if we should highlight
	if GetFlag(namespace + "_HIGHLIGHT_HASH") {
		// return highlighted string
		return highlightHash(str)
	} else {
		// return string with defaultFormat
		return fmt.Sprintf("%s%s%s", normalFormat, str, Reset)
	}

}
