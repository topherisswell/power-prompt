package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"syscall"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/sts"
	smithy "github.com/aws/smithy-go"
	ini "github.com/ochinchina/go-ini"
)

type AWSCredential struct {
	aws_access_key_id     string
	aws_secret_access_key string
}
type AWSProfiles struct {
	Default *AWSCredential `toml:"default"`
}

func writeAWSLine(ctx context.Context, out chan<- string, wg *sync.WaitGroup) (str string) {
	defer wg.Done()
	AWSCredSource, AWSPrincipal, AWSAccountNo := getAWS(ctx)
	AWSCredSourceNamespace := "AWS_CREDSOURCE"
	AWSPrincipalNamespace := "AWS_PRINCIPAL"
	AWSAccountNamespace := "AWS_ACCOUNT"

	if len(AWSCredSource) <= 0 {
		return ""
	}
	var (
		awsCredStr      string
		awsPrincipalStr string
		awsAccountNoStr string
	)
	awsCredStr = FormatHighlightableString(AWSCredSource, AWSCredSourceNamespace, Yellow)
	if len(AWSPrincipal) > 0 {
		// shorten arn if desired
		if GetFlag(AWSPrincipalNamespace + "_SHORT_ARN") {
			// AWS ARN fields are separated by colons ":"
			// we're only interested in everything after the last colon
			// add one to it so we don't include the colon itself
			// if there are no colons, we'll get -1 from lastIndex, and thus
			// lastColon will be zero, so we'll return the whole string
			lastColon := strings.LastIndex(AWSPrincipal, ":") + 1
			// this might panic if it's empty, make sure this is non-zero length
			AWSPrincipal = AWSPrincipal[lastColon:]
		}

		// format string
		awsPrincipalStr = fmt.Sprintf("%s⟶%s %s ", Blue, Reset,
			FormatHighlightableString(AWSPrincipal, AWSPrincipalNamespace, Yellow))

	}

	if len(AWSAccountNo) > 0 {
		// don't display by default
		if GetFlag(AWSAccountNamespace + "_DISPLAY") {
			awsAccountNoStr = fmt.Sprintf("%s⟶%s %s ", Blue, Reset,
				FormatHighlightableString(AWSAccountNo, AWSAccountNamespace, Yellow))
		}
	}
	defer func() {
		str = fmt.Sprintf("%s―[%s %s☁%s %s%s%s%s]%s", Green, Reset, Blue, Reset, awsCredStr, awsAccountNoStr, awsPrincipalStr, Green, Reset)

		out <- str
	}()
	// check context, return if expired
	select {
	case <-ctx.Done():
		return
	default:
	}
	return
}

func lookupAWSID(ctx context.Context) (principal string, accoutNumber string, extraInfo string) {
	// TODO - if DNS is down, this will hang for a long time, might need a custom HTTP client.

	// if we return early, return at least a generic error
	// no retries to ensure this completes quickly
	cfg, err := config.LoadDefaultConfig(ctx, config.WithRetryer(func() aws.Retryer { return aws.NopRetryer{} }))
	if err != nil {
		//fmt.Printf("unable to load SDK config, %v", err)
		extraInfo = "AWSConfigError"
		return
	}

	// Using the Config value, create the DynamoDB client
	svc := sts.NewFromConfig(cfg)
	out, err := svc.GetCallerIdentity(ctx, nil)

	if err != nil {

		//fmt.Printf("unable to get caller id, %v\n", err)
		var ae smithy.APIError
		if errors.As(err, &ae) {
			//log.Printf("code: %s, message: %s, fault: %s\n", ae.ErrorCode(), ae.ErrorMessage(), ae.ErrorFault().String())
			extraInfo = ae.ErrorCode()
		} else if dnsErr, ok := err.(*net.DNSError); ok {
			// order errors from least to most specific so they can be overwritten with more detail
			extraInfo = "DNSError"
			if dnsErr.Temporary() {
				extraInfo = "DNSTempError"

			}

			if dnsErr.Timeout() {
				extraInfo = "DNSTimeout"

			}
			if dnsErr.IsNotFound {
				extraInfo = "DNSNotFound"
			}
		} else if errors.Is(err, syscall.ECONNRESET) {
			extraInfo = "connectionReset"
		} else if errors.Is(err, context.DeadlineExceeded) || os.IsTimeout(err) {
			extraInfo = "timedOut"
		} else {
			extraInfo = "IDLookupFailed"
		}
		return
	}
	if out == nil {
		extraInfo = "EmptyAPIResponse"
		return
	}
	accoutNumber = *out.Account
	principal = *out.Arn
	return
}

func getAWS(ctx context.Context) (credSource string, principal string, accoutNumber string) {
	var accessKey string
	// extra information to append to the principal for additional context
	var fluff string
	defer func() {
		if strings.HasPrefix(accessKey, "ASIA") {
			credSource += " (temp) "
		} else if strings.HasPrefix(accessKey, "AKIA") {
			credSource += " (long-term) "
		}
	}()
	env_access_id := os.Getenv("AWS_ACCESS_KEY_ID")
	if len(env_access_id) > 0 {
		accessKey = env_access_id
		credSource = "ENV"
		env_secret_key := os.Getenv("AWS_SECRET_ACCESS_KEY")
		if len(env_secret_key) <= 0 {
			principal = "Missing Secret Key"
			return
		} else {
			principal = accessKey
			principal, accoutNumber, fluff = lookupAWSID(ctx)
			if len(principal) <= 0 {
				principal = accessKey
			}
			if len(fluff) > 0 {
				principal += " (" + fluff + ")"
			}
			return

		}
	}

	select {
	case <-ctx.Done():
		return
	default:
	}
	// check on disk creds
	awsCredsFile := filepath.Join(homeDir, ".aws", "credentials")

	stat, err := os.Stat(awsCredsFile)
	if err != nil {
		//fmt.Printf("%s", err.Error())
		return
	}
	if !stat.Mode().IsRegular() || stat.Size() < 12 {
		LogError("invalid AWS credentials file")
		return

	}
	awsCredsData, err := os.ReadFile(awsCredsFile)
	if err != nil {
		//fmt.Printf("%s", err.Error())
		return
	}
	awsProfiles := ini.Load(awsCredsData)

	fileAccessId, err := awsProfiles.GetValue("default", "aws_access_key_id")
	if err != nil {
		//fmt.Printf("%s", err.Error())
		return
	}
	accessKey = fileAccessId
	fileSecretKey, err := awsProfiles.GetValue("default", "aws_secret_access_key")
	if err != nil {
		//fmt.Printf("%s", err.Error())
		return
	}
	if len(accessKey) > 0 && len(fileSecretKey) > 0 {
		credSource = "on-disk"
		principal = accessKey
		principal, accoutNumber, fluff = lookupAWSID(ctx)
		if principal == "" {
			principal = accessKey
		}
		if len(fluff) > 0 {
			principal += " (" + fluff + ")"
		}
		return
	}
	// TODO check IMDS

	return
}
