package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	yaml "gopkg.in/yaml.v2"
)

// kubeconfig yaml types
type Cluster struct {
	Server string `yaml:"server,omitempty"`
}

type ClusterListItem struct {
	Cluster Cluster `yaml:"cluster,omitempty"`
	Name    string  `yaml:"name,omitempty"`
}
type Context struct {
	Cluster   string `yaml:"cluster,omitempty"`
	User      string `yaml:"user,omitempty"`
	Namespace string `yaml:"namespace,omitempty"`
}

type ContextListItem struct {
	Context Context `yaml:"context,omitempty"`
	Name    string  `yaml:"name,omitempty"`
}

type kubeconfig struct {
	Clusters       []ClusterListItem `yaml:"clusters,omitempty"`
	Contexts       []ContextListItem `yaml:"contexts,omitempty"`
	CurrentContext string            `yaml:"current-context"`
}

func writeK8sLine(ctx context.Context, out chan<- string, wg *sync.WaitGroup) (str string) {
	defer wg.Done()
	kubeCluster, kubeNamespace, _, _ := getK8s(ctx)
	if len(kubeCluster) <= 0 {
		return ""
	}
	namespaceString := ""
	if len(kubeNamespace) > 0 {
		// check if we should highlight the string
		kubeNamespace = FormatHighlightableString(kubeNamespace, "KUBENS", Yellow)
		namespaceString = fmt.Sprintf("%s⟶%s %s ", Blue, Reset, kubeNamespace)
	}
	// check if we should highlight the string
	kubeCluster = FormatHighlightableString(kubeCluster, "KUBECLUSTER", Yellow)
	str = fmt.Sprintf("%s―[%s %s☸%s %s %s%s]%s", Green, Reset, Blue, Reset, kubeCluster, namespaceString, Green, Reset)

	out <- str
	return
}

func getK8s(ctx context.Context) (cluster string, namespace string, user string, context string) {
	k8sConfigFile = filepath.Join(homeDir, ".kube", "config")

	stat, err := os.Stat(k8sConfigFile)
	if err != nil {
		//fmt.Printf("%s", err.Error())
		return
	}
	if !stat.Mode().IsRegular() || stat.Size() < 12 {
		LogError("invalid kubeconfig file")
		return

	}
	k8sConfigData, err := os.ReadFile(k8sConfigFile)
	if err != nil {
		//fmt.Printf("%s", err.Error())
		return
	}
	var kubeConfig = kubeconfig{}

	// check context, return if expired
	select {
	case <-ctx.Done():
		return
	default:
	}

	err = yaml.Unmarshal([]byte(k8sConfigData), &kubeConfig)

	if err != nil {
		//fmt.Printf("%s", err.Error())
		LogError(fmt.Sprintf("error opening kubeconfig '%s': %v", k8sConfigFile, err))
	}

	if len(kubeConfig.CurrentContext) > 0 {
		for _, i := range kubeConfig.Contexts {
			if i.Name == kubeConfig.CurrentContext {
				namespace = i.Context.Namespace
				cluster = i.Context.Cluster
				user = i.Context.User
				context = i.Name
				return
			}

		}
	}

	return
}
