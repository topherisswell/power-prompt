package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

//tfstate file structs

type tfState struct {
	Backend TfStateBackend `json:"backend,omitempty"`
}
type TfStateBackend struct {
	Type   string        `json:"type"`
	Config TfStateConfig `json:"config"`
}
type TfStateConfig struct {
	Address string `json:"address,omitempty"`
}

func writeTerraformLine(ctx context.Context, out chan<- string, wg *sync.WaitGroup) (str string) {
	defer wg.Done()
	terraformState := getTerraform(ctx)
	if len(terraformState) <= 0 {
		return ""
	}
	str = fmt.Sprintf("%s―[%s %s♻%s %s %s]%s", Green, Reset, Blue, Reset, terraformState, Green, Reset)
	out <- str
	return
}

func getTerraform(ctx context.Context) (str string) {
	defer func() {
		if len(str) > 0 {
			str = FormatHighlightableString(str, "TERRAFORM_STATE", Yellow)
		}
	}()
	tfStateFile := filepath.Join(".terraform", "terraform.tfstate")

	stat, err := os.Stat(tfStateFile)
	if err != nil {
		LogDebug("TF - Unable to stat tfstate file: " + err.Error())
		return
	}

	// check context, return if expired
	select {
	case <-ctx.Done():
		LogError("TF - Context done: " + ctx.Err().Error())
		return
	default:
	}

	if !stat.Mode().IsRegular() || stat.Size() < 12 {
		LogError("invalid tfstate file")
		return

	}
	tfStateData, err := os.ReadFile(tfStateFile)
	if err != nil {

		LogError("TF - Unable to read tfstate file: " + err.Error())
		return
	}

	// check context, return if expired
	select {
	case <-ctx.Done():
		LogError("TF - Context done: " + ctx.Err().Error())
		return
	default:
	}

	var tfState = tfState{}

	err = json.Unmarshal([]byte(tfStateData), &tfState)
	if err != nil {
		LogError("TF - Unable to unmarshal tfstate file: " + err.Error())
	}

	if tfState.Backend.Type == "http" {
		str = tfState.Backend.Config.Address
		str = strings.TrimPrefix(str, "http://")
		str = strings.TrimPrefix(str, "https://")
	} else {
		str = tfState.Backend.Type
	}
	return
}
