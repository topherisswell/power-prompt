package main

import (
	"context"
	"fmt"
	"os"
	"path"
	"strings"
	"sync"

	git "github.com/go-git/go-git/v5"
	plumbing "github.com/go-git/go-git/v5/plumbing"
)

func writeGitLine(ctx context.Context, out chan<- string, wg *sync.WaitGroup) (str string) {
	defer wg.Done()

	var gitRemote, gitHash, gitRef string // := getGit(ctx, "")

	// dumpToChannel() will write whatever information we have to the main thread
	// this is necessary because some operations within go-git can take a long time
	// (think tens of seconds) and will miss the little context cancellation window
	// So, this lets us dump whatever we have and iteratively add more information as
	// it comes in. If we hang on a function, we'll at least have some information.
	dumpToChannel := func(gitRemote string, gitHash string, gitRef string) {

		// There's a lot of redundant empty-string checking here
		// If we become concerned about the performance (which I doubt)
		// we could make this into a matrix lookup, but I'd prefer readable code
		// and I doubt this is complex enough that we could eke out any performance anyway
		if len(gitRemote) <= 0 && len(gitRef) <= 0 && len(gitHash) <= 0 {
			return
		}
		var (
			gitRemoteStr  string
			gitHashStr    string
			gitRefStr     string
			connectorStr1 string
			connectorStr2 string
		)
		// only format full strings, and put a trailing space if not empty
		if len(gitRemote) > 0 {
			gitRemoteStr = FormatHighlightableString(gitRemote, "GITREMOTE", Yellow) + " "
		}
		if len(gitHash) > 0 {
			gitHashStr = FormatHighlightableString(gitHash, "GITHASH", Yellow) + " "
		}
		if len(gitRef) > 0 {
			gitRefStr = FormatHighlightableString(gitRef, "GITREF", Yellow) + " "

		}
		// only print connectors if we have data on both sides
		if len(gitRef) > 0 && len(gitHash) > 0 {
			connectorStr2 = fmt.Sprintf("%s⟶%s ", Blue, Reset)
		}
		if len(gitRef) > 0 && len(gitRemote) > 0 {
			connectorStr1 = fmt.Sprintf("%s⟶%s ", Blue, Reset)
		}

		gitRepoInfo := fmt.Sprintf("%s―[%s %sᛘ%s %s%s%s%s%s%s]%s", Green, Reset, Blue, Reset, gitRemoteStr, connectorStr1, gitHashStr, connectorStr2, gitRefStr, Green, Reset)
		str = gitRepoInfo
		out <- str

	}

	gitRemoteChan := make(chan string)
	gitHashChan := make(chan string)
	gitRefChan := make(chan string)

	var gitWait sync.WaitGroup
	gitWait.Add(1)
	go getGit(ctx, &gitWait, gitRemoteChan, gitHashChan, gitRefChan)

	softWait := SoftWait(&gitWait)

	for {
		select {
		case <-ctx.Done():
			return
		case gitRemote = <-gitRemoteChan:
			dumpToChannel(gitRemote, gitHash, gitRef)
			continue
		case gitHash = <-gitHashChan:
			dumpToChannel(gitRemote, gitHash, gitRef)
			continue
		case gitRef = <-gitRefChan:
			dumpToChannel(gitRemote, gitHash, gitRef)
			continue
		case <-softWait:
			dumpToChannel(gitRemote, gitHash, gitRef)
			return
		}
	}

}

func getGit(ctx context.Context, wg *sync.WaitGroup, originChan chan<- string, githashChan chan<- string, gitrefChan chan<- string) {
	defer wg.Done()
	// check context, return if expired
	select {
	case <-ctx.Done():
		LogError("git - Context done: " + ctx.Err().Error())
		return
	default:
	}

	var origin, githash, gitref string
	// write whatever we have if we exit prematurely
	defer func() {
		originChan <- origin
		githashChan <- githash
		gitrefChan <- gitref
	}()

	dir, err := os.Getwd()
	if err != nil {
		LogError("git - Unable to read get workdir: " + err.Error())
		return
	}

	// loop until we reach a loop, root, or a git directory
	for {
		dirinfo, err := os.Stat(dir)
		if err != nil || !dirinfo.IsDir() {
			LogDebug("git - This is not a dir: '" + dir + "' error: " + err.Error())
			// this isn't a dir or we can't stat it
			// give up
			return
		}

		fileinfo, err := os.Stat(path.Join(dir, git.GitDirName))
		if err == nil && fileinfo.IsDir() {
			// found our git dir
			break
		} else {
			if err != nil {
				LogDebug("git - Unable to stat dir: '" + dir + "' error: " + err.Error())
			} else if !fileinfo.IsDir() {
				LogDebug("git - not a git dir: " + path.Join(dir, git.GitDirName))

			}

			//fmt.Printf("not a git repo: %s\nErr: %v\n", dir, err)
			dir = path.Clean(dir)
			// detect loops or the rootdir
			if strings.Compare(dir, path.Clean(path.Join(dir, ".."))) == 0 {
				LogDebug("git - loop detected traversing directory tree at '" + dir + "' error: " + err.Error())
				// loop or root dir
				// give up
				return
			}

			// try the next directory up next loop
			dir = path.Join(dir, "..")
		}
	}

	LogDebug("Git - Attempting Opening Git Repo at " + dir)
	r, err := git.PlainOpen(dir)
	if err != nil {
		LogError("git.open err: " + err.Error())
		return
	}
	// check context, return if expired
	select {
	case <-ctx.Done():
		LogError("git - Context done: " + ctx.Err().Error())
		return
	default:
	}

	remotes, err := r.Remotes()
	if err != nil {
		LogError(fmt.Sprintf("r.remotes() err: %v", err))
		return
	}
	for _, remote := range remotes {
		if remote.Config().Name == "origin" {
			origin = strings.TrimPrefix(remote.Config().URLs[0], "ssh://")
			origin = strings.TrimPrefix(origin, "git://")
			origin = strings.TrimPrefix(origin, "git@")
			origin = strings.TrimPrefix(origin, "http://")
			origin = strings.TrimPrefix(origin, "https://")

			// write origin
			originChan <- origin
			LogDebug("Git origin is " + origin)
		}
	}

	head, err := r.Head()
	if err != nil {
		// This usually occurs if there are no commits in the repo
		LogError(fmt.Sprintf("Git - head() err: %v", err))
		return
	}

	if head == nil {
		LogError("Git - head ref is nil")
		return
	}

	if head.Hash().IsZero() {
		LogError("Git - head ref is zero")
		return
	}

	// write hash
	githash = fmt.Sprintf("%.9s", head.Hash().String())
	githashChan <- githash
	LogDebug("git Hash is " + githash)

	// write ref
	gitref = strings.TrimPrefix(head.Name().String(), "refs/")
	gitrefChan <- gitref
	LogDebug("git ref is " + gitref)
	if !head.Name().IsBranch() {
		gitref += " detached"
		gitrefChan <- gitref
	}
	// check context, return if expired
	select {
	case <-ctx.Done():
		LogError("git - Context done: " + ctx.Err().Error())
		return
	default:
	}

	refs, err := r.References()
	if err != nil {
		LogError(fmt.Sprintf("Git - r.ref() err: %v", err))
		return
	}
	// TODO - lookup if we're ahead or behind this branch on origin and by how much
	err = refs.ForEach(func(ref *plumbing.Reference) error {
		// check context, return if expired
		select {
		case <-ctx.Done():
			LogError("git - Context done: " + ctx.Err().Error())
			return ctx.Err()
		default:
		}
		if ref.Name().IsTag() {

			//fmt.Println(ref.Name().String() + " " + ref.Hash().String())
			if strings.Compare(head.Hash().String(), ref.Hash().String()) == 0 {
				// there is a tag for this commit
				gitref += " at " + strings.TrimPrefix(ref.Name().Short(), "refs/")
				// this returns the first tag we find (oldest tag for this commit)
				// There may be times when we want the newest (or all of the tags?)
				return nil
			}
			/* FAILED ATTEMPTS
			//parent, err := commit.Parent(1)
			//if err != nil {
			//	fmt.Printf("parent lookup err: %v\n", err)
			//	return nil
			//}
			//fmt.Println("parent: " + parent.Hash.String())

			resolvedRef, err := r.Reference(ref.Name(), false)
			if err != nil {
				fmt.Printf("ref lookup err: %v\n", err)
				return nil
			}
			fmt.Println(resolvedRef.Hash().String())
			obj, err := r.Object(plumbing.ObjectType(ref.Type()), ref.Hash())
			if err != nil {
				fmt.Printf("obj lookup err: %v\n", err)
				return nil
			}
			fmt.Println(obj.ID().String())
			*/

			tagobj, err := r.TagObject(ref.Hash())
			if err != nil {
				LogError(fmt.Sprintf("tag obj lookup err: %v\n", err))
				return fmt.Errorf("Error looking up tag: " + err.Error())
			}
			commit, err := tagobj.Object()
			if err != nil {
				LogError(fmt.Sprintf("commit lookup err: %v\n", err))
				return fmt.Errorf("Error looking up commit: " + err.Error())
			}

			if strings.Compare(head.Hash().String(), commit.ID().String()) == 0 {
				// there is a tag for this commit
				gitref += " at " + strings.TrimPrefix(ref.Name().Short(), "refs/")
				gitrefChan <- gitref
				return nil
			}
		}
		return fmt.Errorf("no references found")
	})
	if err != nil {

		LogDebug("git - reference walk returned: " + err.Error())
	} else {
		LogDebug("git - reference walk returned nil")
	}

	// wait until gitstatus completes (this might be a while)
	var gitStatusWait sync.WaitGroup
	gitStatusWait.Add(1)
	go func() {
		defer gitStatusWait.Done()
		var isClean bool = true

		wt, err := r.Worktree()

		if err != nil {
			LogError("git.worktree err: " + err.Error())

		} else {

			// go-git's worktree.Status() call can take a looong time
			// I've seen it go 15s on a simple repo with a lot of files
			gitstatus, err := wt.Status()
			if err != nil {
				LogError("git.status err: " + err.Error())

			}
			isClean = gitstatus.IsClean()
		}
		var mark rune
		var markColor string
		if isClean {
			mark = '🗸'
			markColor = Green
		} else {
			mark = '🗴'
			markColor = Red
		}

		gitref += fmt.Sprintf(" %s%c%s", markColor, mark, Reset)
		gitrefChan <- gitref
	}()

	// instead of doing gitStatusWait, do this soft wait
	// so that we can return and call the defer clauses
	// otherwise we'll be blocking with worktree.Status
	// goes out to lunch for days
	softWait := SoftWait(&gitStatusWait)
	select {
	case <-ctx.Done():
		return
	case <-softWait:
		return
	}
}
