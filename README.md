# Power Prompt
This program simply prints out a shell prompt, so to use it, simply set your `$PS1` to the output of this command. It exits within 500ms whether or not it has gathered all of the information, and if it reached that timeout it will only display the information that was collected. This avoids allows the program to make API calls to check information, but still return a prompt in a reasonable time (as no one wants to wait for their prompt to appear).

![Picture_of_terminal](busyTerm.png)

It's primary target is a bash prompt on Debian- and RHEL-based systems, but effort is made to make it cross platform and shell-agnostic. 

This software is still in testing and caution is advised in its use. 

It outputs information about your current development context, including:
- Kubernetes Context / Cluster / Namespace
- terraform state file 
- AWS role
- git origin / hash / tag/branch
- IP Address
- Working directory

This saves you a few commands checking what's your current status before committing/applying something and hopefully reduces mistakes.

## Build
This part's tricky, so try to keep up
```bash
go build
```
There. You're done. You should have a `power-prompt` binary.

I'm trying to ensure this project is able to compile with the latest go package on LTS Ubuntu and/or stable Fedora, but the dependencies may break that promise.

#### Note for Alpine users!
I'm not sure what dependency is doing this, but a binary compiled on a GNU/linux distro will not run on alpine. I'm guessing there's CGO in there somewhere, so unlike many go binaries this is not a portable amd64-linux binary, this will specifically target amd64-linux-gnu. 

So, if you have the issue where you get something like `/bin/sh: /bin/power-prompt: not found` it's probably because it's looking for the Glibc ELF interpreter which won't be on Alpine or other musl-based distros.

The simplest way to address this is to compile on Alpine if you're targeting alpine. There's probably a way to cross compile from a GNU distro, it likely involves installing a bunch of packages (probably recompiling you C toolchain) and setting `CC=x86_64-alpine-linux-musl-gcc` but that's out of my pay-grade. 

To compile on a vanilla Alpine box:
```bash
apk add go make git sudo
rm go.*
make
```

## Installation
This assumes bash on a linux system, though I don't see why it wouldn't work on zsh on a Mac, perhaps with some modifications. It _should_ work on a windows box, but I don't know enough about that to say how you would install it. But I make reasonable effort to make the binary at least execute cross-platform.

If there is a variable in the environment named LAST_RC, it will treat that as the return code of the last command and color the prompt symbol red if it indicates an error for the last command.

```bash
sudo install power-prompt /bin
## to try before you buy in your current shell, just run this
export PROMPT_COMMAND="export PS1=\$(/bin/power-prompt)"

## to enable it permanently for all of your prompts, do this
cat << EOF >> ~/.bashrc
export PROMPT_COMMAND="export LAST_RC=\$?; export PS1=\$(/bin/power-prompt)"
EOF
source ~/.bashrc
```

#### Esoteric Shells
For shells that don't support the PROMPT_COMMAND envvar like busybox and Bourne Shell, you would have to set PS1 to be an escaped shell expansion command, that is expanded each time the prompt is printed. IIRC, there are some bugs in this behavior (around the shell counting characters correctly, I think?), but if you want to try it out, I would recommend doing it something like this:

```bash
sudo install power-prompt /bin
## temporary install for current session only:
export PS1="\`/bin/power-prompt\`"
```

To enable it persistently, copy it that command to the shell rc file in your home directory (this will vary by shell) like `.shrc` or `.profile`

## Uninstallation
To uninstall, though I can scarcely imagine why you would want to, remove the added bits from your `.bashrc`. to disable it in your current shell session, just unset the PROMPT_COMMAND and set PS1 to something sane, like so:

```bash
unset PROMPT_COMMAND
export PS1="\u@\h \W\$ "
```

## Configuration
This pays attention to some variables in the environment

#### POWERPROMPT_DEBUG
When set to 1 or 'true', debugging and error messages are printed to STDOUT

#### POWERPROMPT_TIMEOUT_MS
Set to a number (at least 1), and the prompt will do its best to exit within that time (it may take an additional ~3ms past your timeout to clean up if the value is especially low).  
This defaults to 500, which is noticable, but not frustrating (for me, at least). Depending on your hardware, values as low as 25 ought to give you a consistent and reliable experience (though any Internet API information like AWS will not be able to be fetched), less than that and you might not get all components to report in and get an inconsistent prompt.   
A value of 250 should be pretty snappy and give you time to consistently get API-driven information.

## Wishlist
- Highlight text such as "prod" in your states/contexts/repos/branches to draw attention to when you need to be most careful.
- Configurability (what's displayed, icons, colors, etc.)
- Show if git workspace is clean/modified/staged (currently onlys signals if the workspace is clean or not)
- Show if ahead/behind git origin
- Logging (esp. for errors)
- Tests
- Command-line flags, such as printing help or version
- dark-mode / light-mode?
- Detect and signal if we're on a VPN
  

