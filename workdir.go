package main

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/user"
	"strings"
	"sync"
	"time"

	"github.com/distatus/battery"
)

func getIPAddress(ctx context.Context) (str string) {
	IPAddrNamespace := "IPADDR"
	ifaces, err := net.Interfaces()
	if err != nil {
		LogError("ERR polling interfaces: " + err.Error())
		return ""
	}
	displayAddress := ""
	VPNConnected := false

	// format the string for display right before exiting
	// to allow us to exit early
	defer func() {
		// only return a formatted string if there something to display
		if len(displayAddress) > 0 {
			ipAddrStr := FormatHighlightableString(displayAddress, IPAddrNamespace, Blue)
			vpnStr := ""

			if VPNConnected {
				vpnStr = "🗝 "
			}

			str = fmt.Sprintf("%s―[%s %s %s%s]%s", Green, Reset, ipAddrStr, Green, vpnStr, Reset)
		}

	}()

	highscore := 0
	for _, i := range ifaces {
		ifscore := 100
		name := i.Name
		//fmt.Printf("%s \tInterface #%d\n", i.Name, i.Index)
		if strings.HasPrefix(name, "wlan") || strings.HasPrefix(name, "eth") || strings.HasPrefix(name, "enp") || strings.HasPrefix(name, "wlp") {
			ifscore += 11
		}
		if strings.HasPrefix(name, "net") || strings.HasPrefix(name, "wifi") {
			ifscore += 7
		}
		if strings.HasPrefix(name, "virbr") || strings.HasPrefix(name, "br") || strings.HasPrefix(name, "vnet") || strings.HasPrefix(name, "sw") ||
			strings.HasPrefix(name, "virt") || strings.HasPrefix(name, "docker") || strings.HasPrefix(name, "vm") {
			ifscore -= 5
		}
		if strings.HasPrefix(name, "lo") {
			ifscore /= 2
		}
		//fmt.Println("Flags: " + i.Flags.String())
		flags := i.Flags
		if flags&net.FlagUp != 0 {
			ifscore *= 3
		}
		if flags&net.FlagLoopback != 0 {
			ifscore /= 2
		}

		addrs, err := i.Addrs()
		if err != nil {
			LogError("ERR getting interface address: " + err.Error())
			return ""
		}
		if strings.HasPrefix(name, "vpn") || strings.HasPrefix(name, "wg") || strings.HasPrefix(name, "tun") || strings.HasPrefix(name, "utun") {
			if i.Flags&net.FlagUp != 0 && len(addrs) > 0 {
				// interface has a VPN-like name, it's up and it has at least one IP address assigned.
				// For now this is good enought to mark us as connected to VPN
				// TODO: a envvar should define a VPN resource that we can ping (but ping in an unprivileged and X-platform way)
				//			to more confidently determine if we're VPN-connected.
				VPNConnected = true
			}
			ifscore += 4
		}
		for _, a := range addrs {
			// check context, return if expired
			select {
			case <-ctx.Done():
				return
			default:
			}
			addr := a.String()
			// the score for this IP Address starts with the interface score
			score := ifscore
			if strings.HasPrefix(addr, "169.254.") || strings.HasPrefix(addr, "192.88.99.") || strings.HasPrefix(addr, "fe80::") {
				score -= 15
			}
			if strings.HasPrefix(addr, "22") {
				score -= 9
			}
			if strings.HasPrefix(addr, "22.") || strings.HasPrefix(addr, "23.") || strings.HasPrefix(addr, "24.") || strings.HasPrefix(addr, "25.") {
				score += 11
			}
			if strings.HasPrefix(addr, "10.") || strings.HasPrefix(addr, "192.168.") || strings.HasPrefix(addr, "172.2") || strings.HasPrefix(addr, "fc00::") {
				score -= 8
			}
			if strings.HasPrefix(addr, "172.1") || strings.HasPrefix(addr, "172.3") {
				score -= 4
			}
			if strings.HasPrefix(addr, "0.") || strings.HasPrefix(addr, "192.0.0.") || strings.HasPrefix(addr, "192.0.2.") || strings.HasPrefix(addr, "198.58.100.") ||
				strings.HasPrefix(addr, "203.0.113.") || strings.HasPrefix(addr, "233.252.") || strings.HasPrefix(addr, "2001:db8::") {
				score -= 19
			}
			if strings.HasPrefix(addr, "100.") || strings.HasPrefix(addr, "198.18.") || strings.HasPrefix(addr, "198.19.") || strings.HasPrefix(addr, "23") ||
				strings.HasPrefix(addr, "24") || strings.HasPrefix(addr, "25") || strings.HasPrefix(addr, "::") || strings.HasPrefix(addr, "ff00::") {
				score -= 11
			}
			if strings.Contains(addr, ":") {
				score /= 2
			}
			if strings.HasPrefix(addr, "127.") || strings.HasPrefix(addr, "::1") {
				score /= 3
			}
			if strings.HasPrefix(addr, "255.255.255.255") {
				score /= 10
			}

			//fmt.Printf("%s (%s) [%d], \t", a.String(), a.Network(), score)
			if score > highscore {
				displayAddress = addr
				highscore = score
			}

		}

	}
	// check context, return if expired
	select {
	case <-ctx.Done():
		return
	default:
	}

	return
}

func getTime() (str string) {
	localtime := time.Now().Local()
	clock := localtime.Format("15:04:05")
	// 0x01f550 is the symbol for 1 o'clock,
	// with subsequent characters being subsequent hours
	clockSymbol := 0x01f54f + localtime.Hour()%12
	// 12 o'clock is hour 0, but is the 11th index of the symbols
	if clockSymbol < 0x01f550 {
		clockSymbol += 12
	}

	if localtime.Minute() > 15 {
		// use the time-thirty clock symbols offset by 12
		clockSymbol += 12
	}

	if localtime.Minute() > 45 && localtime.Hour()%12 != 0 {
		// use the o'clock for the next hour (and undo the switch to time-thirty)
		clockSymbol -= 11
	} else if localtime.Minute() > 45 && localtime.Hour()%12 == 0 {
		// if its between 12:45 and 1:00, call it one o'clock
		clockSymbol = 0x01f550

	}

	str = fmt.Sprintf("%s―[%s %c %s%s%s %s]%s", Green, Reset, clockSymbol, Blue, clock, Reset, Green, Reset)
	return

}

// TODO
func getBattery() (str string) {
	batteries, err := battery.GetAll()
	if err != nil {
		LogError("Wkdir - GetBattery failed: " + err.Error())
		return
	}
	if batteries == nil {
		LogDebug("Wkdir - No batteries found")
		return
	}
	CurrentCapacity := 0.0
	FullCapacity := 0.0
	for _, batt := range batteries {
		if batt.State == battery.Charging {
			str += "🗲"
		}
		CurrentCapacity += batt.Current
		FullCapacity += batt.Full
	}
	if FullCapacity < 0.1 {
		LogError("wkdir - Battery capacity < 0.1 -- Omitting")
		return
	}
	percentCapacity := CurrentCapacity / FullCapacity * 100.0
	batteryColor := Green
	if percentCapacity < 66 {
		batteryColor = Yellow
	}
	if percentCapacity < 33 {
		batteryColor = Red
	}
	str += fmt.Sprintf("🔋%s%.0f%%%s ", batteryColor, CurrentCapacity/FullCapacity*100.0, Reset)
	return
}

func writeHostInfoLine(ctx context.Context, out chan<- string, wg *sync.WaitGroup) (str string) {
	defer wg.Done()
	str = getTime() + getIPAddress(ctx) + getHostInfo(ctx)
	out <- str
	return str
}
func getHostInfo(ctx context.Context) (str string) {
	// TODO - Use PS1 escapes instead like \u@\h:\w or whatever and test if that's faster (and multiplatform)
	// 			OR replace workingDir $HOME with `~`
	str = fmt.Sprintf("%s―[%s %s%s%s@%s%s%s:%s%s %s]%s", Green, Reset, getBattery(), getUser(ctx), Green, Reset, getHostName(ctx), Green, Reset, getWorkingDir(ctx), Green, Reset)
	return
}
func getHostName(ctx context.Context) (str string) {
	hostnameNamespace := "HOSTNAME"
	hostname, err := os.Hostname()
	if err != nil {
		LogError("Unable to get hostname: " + err.Error())
		return
	}
	str = FormatHighlightableString(hostname, hostnameNamespace, Blue)

	return
}

func getUser(ctx context.Context) (str string) {
	usernameNamespace := "USERNAME"
	usr, err := user.Current()
	if err != nil {
		LogError("unable to get current user: " + err.Error())
		return
	}
	str = FormatHighlightableString(usr.Username, usernameNamespace, Blue)
	return
}
func getWorkingDir(ctx context.Context) (str string) {
	workdirNamespace := "WORKDIR"
	wd, err := os.Getwd()
	if err != nil {
		LogError("Unable to get working dir: " + err.Error())
		return
	}

	defer func() {
		if len(wd) > 0 {
			// we have something to return
			if len(str) <= 0 {
				// the return string has not been formatted yet
				str = FormatHighlightableString(wd, workdirNamespace, Blue)
			}
		}
	}()
	// check context, return if expired
	select {
	case <-ctx.Done():
		return
	default:
	}
	homedir, err := os.UserHomeDir()
	if err != nil {
		LogError("Unable to get user home dir: " + err.Error())
	} else {
		if strings.HasPrefix(wd, homedir) {
			// substitute $HOME with `~` for conciseness
			wd = "~" + strings.TrimPrefix(wd, homedir)
		}
	}
	return

}
