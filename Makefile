BINARY_NAME = power-prompt
SRCS = main.go aws.go k8s.go git.go terraform.go workdir.go
DEPFILES = go.mod go.sum
BUILD_TIME=`date +%FT%T%z`
GIT_REVISION=`git rev-parse --short HEAD`
GIT_BRANCH=`git rev-parse --symbolic-full-name --abbrev-ref HEAD`
GIT_DIRTY=`git diff-index --quiet HEAD -- || echo "✗-"`
LDFLAGS=-ldflags "-extldflags '-static' -s -X main.appBuildTime=${BUILD_TIME} -X main.appGitHash=${GIT_DIRTY}${GIT_REVISION} -X main.appGitBranch=${GIT_BRANCH}"
GO111MODULE=on

.PHONY: build run clean update-deps install


default: build

build: ${BINARY_NAME}

${BINARY_NAME}: ${SRCS} ${DEPFILES}
	go build ${LDFLAGS} -o ${BINARY_NAME}
 
run: build
	printf "`./${BINARY_NAME}`"

go.mod:
	go mod init gitlab.com/topherisswell/power-prompt

go.sum: go.mod
	go mod tidy

update-deps: go.sum
	go get ./...
	go mod tidy
 
clean:
	go clean
	#rm ${BINARY_NAME}

install: ${BINARY_NAME}
	sudo install power-prompt /bin
